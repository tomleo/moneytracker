# MoneyTracker
Track your spending

A FLOSS app for tracking your spending and savings.
- No integration with Bank Accounts
- No adds
- Host on your own server
- Mobile first PWA approach

# Install

## System Dependencies

``` console
sudo apt-get install python3-pip \
                     python3-dev \
                     libpq-dev \
                     postgresql \
                     postgresql-contrib \
                     gunicorn
```

## Database

sudo -u postgres psql

``` psql
CREATE DATABASE moneytracker;
CREATE USER mt WITH PASSWORD '';
ALTER ROLE mt SET client_encoding TO 'utf8';
ALTER ROLE mt SET default_transaction_isolation TO 'read committed';
ALTER ROLE mt SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE moneytracker TO mt;
\q
```


