
# Supported Database Options
- `django.db.backends.postgresql_psycopg2`
- `django.contrib.gis.db.backends.postgis`
- `django.db.backends.sqlite3`

The only tested DB is the standard postgresql databse engine (with psycopg2)
