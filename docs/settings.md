# Settings

## Secrets

The `settings.py` file loads will load configuration options and secrets via a cson
([CoffeeScript-Object-Notation](https://github.com/bevry/cson) file 

Config options

``` cson
{
    "engine": "django.db.backends.postgresql_psycopg2",
    "name": "mt",
    "user": "dev",
    "password": "dev",
    "host": "127.0.0.1",
    "port": "5432",
    "secret_key": "asdf",
    "debug": false,
    "static_root":  "/http/static/folder",
    "static_url": "/static/",
    "log_file": "/var/log/mt.log"
}
```

### Generae a secure secret key

``` python
# sec_key_gen.py
import sys
import random import SystemRandom
import string

def get_random_string():
    valid_chars = "{}{}{}".format(string.ascii_letters, string.digits, string.punctuation)
    return "".join([SystemRandom().choice(valid_chars) for i in range(50)])

if __name__ == '__main__':
    sys.stdout.write(get_random_string())
```

