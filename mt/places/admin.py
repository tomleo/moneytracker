from django.contrib import admin

from .models import Category, Note, Place

admin.site.register(Category, admin.ModelAdmin)
admin.site.register(Note, admin.ModelAdmin)
admin.site.register(Place, admin.ModelAdmin)
