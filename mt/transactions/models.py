from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from slugify import slugify

from places.models import Place

CASH = 'cash'
SAVINGS = 'savings'
CHECKING = 'checking'
CREDIT = 'credit'
PAYPAL = 'paypal'
VENMO = 'venmo'
BITCOIN = 'bitcoin'
DESTINATION = 'destination'
REV_TYPE = [
    (CASH, 'Cash'),
    (SAVINGS, 'Savings Account'),
    (CHECKING, 'Checking Account'),
    (CREDIT, 'Credit Card'),
    (PAYPAL, 'PayPal Account'),
    (VENMO, 'Venmo Account'),
    (BITCOIN, 'Bitcoin Account'),
]


class Account(models.Model):
    """A User's Account"""
    name = models.CharField(max_length=255)
    account_type = models.CharField(max_length=255, choices=REV_TYPE, blank=True, default=u'')
    person = models.ForeignKey(User, blank=True, null=True)
    balance = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.name, self.account_type.capitalize())


PERSON = 'person'
ACCOUNT = 'account'
PLACE = 'place'
SOURCE_TYPE = [
    (PERSON,  'person'),
    (ACCOUNT, 'account'),
    (PLACE, 'place')
]


class Entity(models.Model):
    """The source or destination of a transaction

    Can either be a person or a place (such as a resturant)
    """
    name = models.CharField(max_length=255)
    source_type = models.CharField(choices=SOURCE_TYPE, max_length=255)

    person = models.ForeignKey(User, blank=True, null=True)
    place = models.ForeignKey(Place, blank=True, null=True)
    account = models.ForeignKey(Account, blank=True, null=True)

    def __str__(self):
        if self.person and self.person.first_name:
            return self.person.first_name
        elif self.account and self.account.name:
            return self.account.name
        elif self.place and self.palce.name:
            return self.place.name
        return self.name


def handle_receipt_upload(instance, filename):
    # import pdb; pdb.set_trace()
    _place = 'uncategorized'
    _date = timezone.now().strftime('%Y-%m-%d')
    # _date = instance.timestamp.strftime('%Y-%m-%d')
    if instance.place and instance.place.name:
        _place = slugify(instance.place.name)
    return 'receipt/%(date)s--%(place)s/%(fname)s' % {
        'date': _date,
        'place': _place,
        'fname': filename
    }


class Receipt(models.Model):
    description = models.TextField(blank=True, default=u'')
    receipt = models.ImageField(blank=True, null=True, upload_to=handle_receipt_upload)
    receipt_text = models.TextField(blank=True, default=u'')  # TODO: populate via OCR
    place = models.ForeignKey(Place, blank=True, null=True)

    def __str__(self):
        return "%s" % self.description


PURCHASE = 'pur'
LOAN = 'loan'
PAYMENT = 'payment'
TRANSACTION_TYPES = [
    (PAYMENT, 'Payment'),
    (PURCHASE, 'Purchase'),
    (LOAN, 'Loan'),
]


class Transaction(models.Model):
    source = models.ForeignKey(Entity, related_name='sources')
    dest = models.ForeignKey(Entity, related_name='dests')
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    transaction_type = models.CharField(max_length=60, choices=TRANSACTION_TYPES, default=PURCHASE)

    note = models.TextField(blank=True, null=True)
    receipt = models.ForeignKey(Receipt, blank=True, null=True)

    # Only one transaction from source to destination can happen
    # in a given datetime
    guid = models.CharField(max_length=255, blank=True, default=u'')
    timestamp = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.guid:
            self.guid = '%s|%s' % (self.source_id, self.dest_id)
        super(Transaction, self).save(*args, **kwargs)

    def __str__(self):
        return '$%(amount)s from %(source_name)s to %(dest_name)s' % {
            'amount': self.amount,
            'source_name': self.source.name,
            'dest_name': self.dest.name,
        }

    class Meta:
        ordering = ('timestamp',)
        unique_together = ('guid', 'timestamp')
