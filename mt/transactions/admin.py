from django.contrib import admin

from .models import Account, Entity, Receipt, Transaction


class TransactionAdmin(admin.ModelAdmin):
    # fields = ['source', 'dest', 'amount', 'note', 'transaction_type']
    list_display = ['__str__', 'timestamp']

admin.site.register(Account, admin.ModelAdmin)
admin.site.register(Entity, admin.ModelAdmin)
admin.site.register(Receipt, admin.ModelAdmin)
admin.site.register(Transaction, TransactionAdmin)
